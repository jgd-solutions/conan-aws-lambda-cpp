from conans import ConanFile, CMake, tools
import os


class AwsLambdaCppConan(ConanFile):
    name = "aws-lambda-cpp"
    description = "C++ implementation of the lambda runtime API"
    topics = ("conan", "aws-lambda-cpp", "aws-lambda", "aws", "lambda")
    homepage = "https://github.com/awslabs/aws-lambda-cpp"
    license = "Apache-2.0"
    exports_sources = ["CMakeLists.txt", "patches/*"]
    generators = "cmake_find_package_multi"

    settings = "os", "arch", "compiler", "build_type"
    options = {"shared": [True, False], "fPIC": [True, False], "lto": [True, False]}
    default_options = {"shared": False, "fPIC": True, "lto": True}
    requires = "libcurl/7.80.0"

    _source_subfolder = "source_subfolder"
    _build_subfolder = "build_subfolder"
    _cmake = None

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        tools.get(**self.conan_data["sources"][self.version])
        extracted_dir = self.name + "-" + self.version
        os.rename(extracted_dir, self._source_subfolder)

        if "patches" in self.conan_data and self.version in self.conan_data["patches"]:
            for patch in self.conan_data["patches"][self.version]:
                tools.patch(**patch)

    def _configure_cmake(self):
        if not self._cmake:
            self._cmake = CMake(self)
            self._cmake.definitions["ENABLE_LTO"] = self.options.lto
            self._cmake.definitions["ENABLE_TESTS"] = False
            self._cmake.configure(build_folder=self._build_subfolder)
        return self._cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        self.copy(pattern="LICENSE", dst="licenses", src=self._source_subfolder)
        self.copy(pattern="NOTICE", dst="licenses", src=self._source_subfolder)
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        # override naming to follow AWS's CMake conventions
        self.cpp_info.filenames["cmake_find_package"] = "aws-lambda-runtime"
        self.cpp_info.filenames["cmake_find_package_multi"] = "aws-lambda-runtime"

        self.cpp_info.names["cmake_find_package"] = "AWS"
        self.cpp_info.names["cmake_find_package_multi"] = "AWS"

        self.cpp_info.components["aws-lambda-runtime"].libs = tools.collect_libs(self)
        self.cpp_info.components["aws-lambda-runtime"].requires = ["libcurl::curl"]
        self.cpp_info.components["aws-lambda-runtime"].build_modules = {
            "cmake_find_package": [
                "lib/aws-lambda-runtime/cmake/aws-lambda-runtime-config.cmake"
            ],
            "cmake_find_package_multi": [
                "lib/aws-lambda-runtime/cmake/aws-lambda-runtime-config.cmake"
            ],
        }
