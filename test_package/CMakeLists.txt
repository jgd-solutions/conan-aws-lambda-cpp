cmake_minimum_required(VERSION 3.1.0)
project(test_package)

find_package(aws-lambda-runtime CONFIG REQUIRED)

add_executable(${PROJECT_NAME} test_package.cpp)
target_link_libraries(${PROJECT_NAME} AWS::aws-lambda-runtime)

aws_lambda_package_target(${PROJECT_NAME})
